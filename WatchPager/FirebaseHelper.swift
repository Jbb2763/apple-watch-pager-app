//
//  FirebaseHelper.swift
//
//  Created by Scott Brower, AppSwage LLC on 11/26/16.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseCore
import FirebaseStorage
import FirebaseDatabase


@objc protocol FirebaseHelperProtocol
{
    @objc optional func firebaseDidReturn(returnDict:NSDictionary?, extraDict:[String:String]?, name:String?)
    @objc optional func firebaseDidReturnImage(returnImage:UIImage?, extraDict:[String:String]?)
    @objc optional func firebaseDidReturnVideo(videoURL:URL?, extraDict:[String:String]?)
}

class FirebaseHelper: NSObject
{
    //MARK: - Read records
    
    static func observeRecords(withPath path:String, forObservation observation:DataEventType, usingProtocol helperProtocol:FirebaseHelperProtocol, andName name:String?)
    {
        let ref = Database.database().reference()
        
        ref.child(path).observe(observation, with: {(snapshot) in
            if let returnDict = snapshot.value as? NSDictionary
            {
                helperProtocol.firebaseDidReturn?(returnDict: returnDict, extraDict: nil, name: name)
            }
            else
            {
                helperProtocol.firebaseDidReturn?(returnDict: nil, extraDict: nil, name: name)
            }
        })
    }
    
    static func observeSingleRecords(withPath path:String, forObservation observation:DataEventType, usingProtocol helperProtocol:FirebaseHelperProtocol, withExtra extraDict:[String:String]?, andName name:String?)
    {
        let ref = Database.database().reference()
        
        ref.child(path).observeSingleEvent(of: observation, with: {(snapshot) in
            if let returnDict = snapshot.value as? NSDictionary
            {
                helperProtocol.firebaseDidReturn?(returnDict: returnDict, extraDict: extraDict, name:name)
            }
            else if let returnArray = snapshot.value as? NSArray
            {
                helperProtocol.firebaseDidReturn?(returnDict: ["array":returnArray], extraDict: extraDict, name:name)
            }
            else
            {
                helperProtocol.firebaseDidReturn?(returnDict: nil, extraDict: extraDict, name:name)
            }
        })
    }
    
    static func observeSingleRecordsUsingFilter(filterKey:String, filterValue:String, withPath path:String, forObservation observation:DataEventType, usingProtocol helperProtocol:FirebaseHelperProtocol, withExtra extraDict:[String:String]?, andName name:String?)
    {
        let ref = Database.database().reference()
        
        ref.child(path).queryOrdered(byChild: filterKey).queryEqual(toValue: filterValue).observeSingleEvent(of: observation, with: {(snapshot) in
            
            if let returnDict = snapshot.value as? NSDictionary
            {
                helperProtocol.firebaseDidReturn?(returnDict: returnDict, extraDict: extraDict, name:name)
            }
            else if let returnArray = snapshot.value as? NSArray
            {
                helperProtocol.firebaseDidReturn?(returnDict: ["array":returnArray], extraDict: extraDict, name:name)
            }
            else
            {
                helperProtocol.firebaseDidReturn?(returnDict: nil, extraDict: extraDict, name:name)
            }
        })
    }

    
    //MARK: - Write records
    
    static func writeRecord(withPath path:String, forData dataDict:[String:Any], usingProtocol helperProtocol:FirebaseHelperProtocol)
    {
        let ref = Database.database().reference()
        
        ref.child(path).setValue(dataDict) { (error, ref) in
            if error != nil
            {
                helperProtocol.firebaseDidReturn?(returnDict: ["error":error.debugDescription], extraDict: nil, name: nil)
            }
            else
            {
                helperProtocol.firebaseDidReturn?(returnDict: ["success":"success"], extraDict: nil, name: nil)
            }
        }
    }
    
    static func updateRecord(withPath path:String, forData dataDict:NSDictionary, usingProtocol helperProtocol:FirebaseHelperProtocol)
    {
        let ref = Database.database().reference()
        ref.child(path).updateChildValues(dataDict as! [AnyHashable : Any])
    }
    
    //MARK: - Delete records
    
    static func deleteRecord(withPath path:String, usingProtocol helperProtocol:FirebaseHelperProtocol)
    {
        let ref = Database.database().reference()
        ref.child(path).removeValue()
    }
    
    //MARK: - Read binary
    
    static func getImage(forPath path:String, usingProtocol helperProtocol:FirebaseHelperProtocol, withExtra extraDict:[String:String]?)
    {
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: path)
        
        storageRef.getData(maxSize: 2048*2048) { (data, error) in
            if error != nil
            {
                helperProtocol.firebaseDidReturnImage!(returnImage: nil, extraDict: extraDict)
            }
            else
            {
                helperProtocol.firebaseDidReturnImage!(returnImage: UIImage(data: data!)!, extraDict: extraDict)
            }
        }
    }
    
    static func getVideo(forPath path:String, usingProtocol helperProtocol:FirebaseHelperProtocol, withExtra extraDict:[String:String]?)
    {
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: path)
        
        var tempPath = NSTemporaryDirectory()
        tempPath = tempPath.appending("results.mov")
        deleteFile(atPath: tempPath)
        let tempURLPath = URL(fileURLWithPath: tempPath)
        
        storageRef.write(toFile: tempURLPath, completion: { (fileURL, error) in
            helperProtocol.firebaseDidReturnVideo!(videoURL: fileURL, extraDict: extraDict)
        })
    }
    
    //MARK: - Write Binary
    
    static func writeImage(image:UIImage, atPath path:String)
    {
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: path)

        let imageData = UIImagePNGRepresentation(image)
        _ = storageRef.putData(imageData!, metadata: nil, completion: { (metadata, error) in
            if (error != nil)
            {
                print (error!.localizedDescription)
            }
        })
    }
    
    
    //MARK:- Utilities
    
    static func getUserID() -> String?
    {
        return Auth.auth().currentUser?.uid
    }
    
    static func changePassword(newPassword:String, usingProtocol helperProtocol:FirebaseHelperProtocol, andName name:String?)
    {
        let user = Auth.auth().currentUser
        user?.updatePassword(to: newPassword, completion: { (error) in
            if error != nil
            {
                helperProtocol.firebaseDidReturn!(returnDict: ["error":error!.localizedDescription], extraDict: nil, name: name)
            }
            else
            {
                helperProtocol.firebaseDidReturn!(returnDict: ["success":"success"], extraDict: nil, name: name)
            }
        })
    }
    
    static func createUser(email:String, withPassword password:String, usingProtocol helperProtocol:FirebaseHelperProtocol, andName name:String?)
    {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if user == nil
            {
                helperProtocol.firebaseDidReturn!(returnDict: ["error":error!.localizedDescription], extraDict: nil, name: name)
            }
            else
            {
                helperProtocol.firebaseDidReturn!(returnDict: ["success":"success"], extraDict: nil, name: name)
            }
        })
    }
    
    static func getAutoID() -> String?
    {
        let ref = Database.database().reference()
        let uniqueKey = ref.childByAutoId()
        return uniqueKey.key
    }
    
    static func signOut()
    {
        //FBSDKLoginManager().logOut()
        try! Auth.auth().signOut()
    }
    
    static private func deleteFile(atPath path:String)
    {
        let fileManager = FileManager.default
        
        do
        {
            try fileManager.removeItem(atPath: path)
        }catch{
            print (error.localizedDescription)
        }
    }
    
    static func removeObserver(withPath path:String)
    {
        Database.database().reference().child(path).removeAllObservers()
    }
}
