//
//  DashboardVC.swift
//  WatchPager
//
//  Created by Scott Brower, AppSwage LLC on 6/1/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController, FirebaseHelperProtocol, OpUpdateProtocol, WatchCommsProtocol
{
    @IBOutlet weak var op1ImageView: UIImageView!
    @IBOutlet weak var op2ImageView: UIImageView!
    @IBOutlet weak var op3ImageView: UIImageView!
    @IBOutlet weak var op4ImageView: UIImageView!
    @IBOutlet weak var op5ImageView: UIImageView!
    @IBOutlet weak var op6ImageView: UIImageView!
    @IBOutlet weak var op7ImageView: UIImageView!
    @IBOutlet weak var op8ImageView: UIImageView!
    @IBOutlet weak var op9ImageView: UIImageView!
    
    @IBOutlet weak var op1MsgView: UIImageView!
    @IBOutlet weak var op2MsgView: UIImageView!
    @IBOutlet weak var op3MsgView: UIImageView!
    @IBOutlet weak var op4MsgView: UIImageView!
    @IBOutlet weak var op5MsgView: UIImageView!
    @IBOutlet weak var op6MsgView: UIImageView!
    @IBOutlet weak var op7MsgView: UIImageView!
    @IBOutlet weak var op8MsgView: UIImageView!
    @IBOutlet weak var op9MsgView: UIImageView!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondRowTopContraint: NSLayoutConstraint!
    @IBOutlet weak var thirdRowTopContraint: NSLayoutConstraint!
    @IBOutlet weak var legendStackView: UIStackView!
    @IBOutlet weak var legendLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var legendTopContraint: NSLayoutConstraint!
    
    var op1TimerLabel = UILabel()
    var op2TimerLabel = UILabel()
    var op3TimerLabel = UILabel()
    var op4TimerLabel = UILabel()
    var op5TimerLabel = UILabel()
    var op6TimerLabel = UILabel()
    var op7TimerLabel = UILabel()
    var op8TimerLabel = UILabel()
    var op9TimerLabel = UILabel()
    
    var opsDict = [String:OpsPhoneModel]()
    var myOffice:String! {
        didSet{
            registerAPNS()
            getOpStatus()
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.opUpdateDelegate = self
        
        WatchCommsModel.sharedWatchComms.addDelegate(self)
        
        let alertController = UIAlertController(title: "APNS Status", message: appDelegate.apnsStatus.rawValue, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            let notificationName = Notification.Name("apnsRegistered")
            NotificationCenter.default.addObserver(self, selector: #selector(self.registerAPNS), name: notificationName, object: nil)
            
            self.topConstraint.constant += self.navigationController!.navigationBar.frame.size.height
            
            self.loadOpsModel()
        }
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        adjustLayoutForPlus()
    }
    
    func loadOpsModel()
    {
        let ops1Model = OpsPhoneModel()
        ops1Model.opName = "OP 1"
        ops1Model.opImageView = self.op1ImageView
        ops1Model.opMessageView = self.op1MsgView
        ops1Model.opTimerLabel = self.op1TimerLabel
        self.opsDict["op1"] = ops1Model
        
        let ops2Model = OpsPhoneModel()
        ops2Model.opName = "OP 2"
        ops2Model.opImageView = self.op2ImageView
        ops2Model.opMessageView = self.op2MsgView
        ops2Model.opTimerLabel = self.op2TimerLabel
        self.opsDict["op2"] = ops2Model
        
        let ops3Model = OpsPhoneModel()
        ops3Model.opName = "OP 3"
        ops3Model.opImageView = self.op3ImageView
        ops3Model.opMessageView = self.op3MsgView
        ops3Model.opTimerLabel = self.op3TimerLabel
        self.opsDict["op3"] = ops3Model
        
        let ops4Model = OpsPhoneModel()
        ops4Model.opName = "OP 4"
        ops4Model.opImageView = self.op4ImageView
        ops4Model.opMessageView = self.op4MsgView
        ops4Model.opTimerLabel = self.op4TimerLabel
        self.opsDict["op4"] = ops4Model
        
        let ops5Model = OpsPhoneModel()
        ops5Model.opName = "OP 5"
        ops5Model.opImageView = self.op5ImageView
        ops5Model.opMessageView = self.op5MsgView
        ops5Model.opTimerLabel = self.op5TimerLabel
        self.opsDict["op5"] = ops5Model
        
        let ops6Model = OpsPhoneModel()
        ops6Model.opName = "OP 6"
        ops6Model.opImageView = self.op6ImageView
        ops6Model.opMessageView = self.op6MsgView
        ops6Model.opTimerLabel = self.op6TimerLabel
        self.opsDict["op6"] = ops6Model
        
        let ops7Model = OpsPhoneModel()
        ops7Model.opName = "OP 7"
        ops7Model.opImageView = self.op7ImageView
        ops7Model.opMessageView = self.op7MsgView
        ops7Model.opTimerLabel = self.op7TimerLabel
        self.opsDict["op7"] = ops7Model
        
        let ops8Model = OpsPhoneModel()
        ops8Model.opName = "OP 8"
        ops8Model.opImageView = self.op8ImageView
        ops8Model.opMessageView = self.op8MsgView
        ops8Model.opTimerLabel = self.op8TimerLabel
        self.opsDict["op8"] = ops8Model
        
        let ops9Model = OpsPhoneModel()
        ops9Model.opName = "OP 9"
        ops9Model.opImageView = self.op9ImageView
        ops9Model.opMessageView = self.op9MsgView
        ops9Model.opTimerLabel = self.op9TimerLabel
        self.opsDict["op9"] = ops9Model
        
        initOps()
    }
    
    func initOps()
    {
        for (_,opsModel) in self.opsDict
        {
            opsModel.setEmptyOp()
        }
        
        getOffices()
    }
    
    //MARK: - UI
    
    @IBAction func menuButtonTapped(_ sender: UIBarButtonItem)
    {
        FirebaseHelper.signOut()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoginVC")
        self.present(controller, animated: true, completion: nil)
    }
    
    func updateOps(opsArray:[OpStatus])
    {
        var opsDictArray=[[String:Any]]()
        
        for opStatus in opsArray
        {
            if let opModel = self.opsDict[opStatus.id.lowercased()]
            {
                opModel.opStatus = opStatus
            }
            
            opsDictArray.append(OpStatus.strucToDict(opStatus: opStatus))
        }
        
        WatchCommsModel.sharedWatchComms.sendData(["opStatus":opsDictArray])
    }
    
    //MARK: - Firebase
    
    func getOpStatus()
    {
        FirebaseHelper.observeRecords(withPath: "Operatories/\(self.myOffice!)", forObservation: .value, usingProtocol: self, andName: "opStatus")
    }
    
    func getOpStatusSnapshot()
    {
        FirebaseHelper.observeSingleRecords(withPath: "Operatories/\(self.myOffice!)", forObservation: .value, usingProtocol: self, withExtra: nil, andName: "opStatus")
    }
    
    func getOffices()
    {
        if let userID = FirebaseHelper.getUserID()
        {
            FirebaseHelper.observeSingleRecordsUsingFilter(filterKey: "userId", filterValue: userID, withPath: "Permissions", forObservation: .value, usingProtocol: self, withExtra: nil, andName: "offices")
        }
    }
    
    //MARK: - Firebase Delegates
    
    
    func firebaseDidReturn(returnDict: NSDictionary?, extraDict: [String : String]?, name: String?)
    {
        if returnDict != nil
        {
            if name == "opStatus"
            {
                parseOpStatus(opStatusDict: returnDict!)
            }
            else if name == "offices"
            {
                parseOffices(officeDict: returnDict!)
            }
        }
    }
    
    //MARK: - Parse Firebase Return
    
    func parseOpStatus(opStatusDict:NSDictionary)
    {
        var opsArray = [OpStatus]()
        
        for (_,value) in opStatusDict
        {
            let opDict = value as! [String:Any]
            let opStruct = OpStatus(id: opDict["id"]! as! String, procedureCode: opDict["currentProcedureCode"] as! String, procedureDescription: opDict["currentProcedureDescription"] as! String, status: opDict["currentStatus"]! as! String, updated: opDict["lastUpdated"]! as! Double)
            opsArray.append(opStruct)
        }
        
        if opsArray.count > 0
        {
            updateOps(opsArray: opsArray)
        }
    }
    
    func parseOffices(officeDict:NSDictionary)
    {
        for (_,value) in officeDict
        {
            let valueDict = value as! [String:String]
            if let officeID = valueDict["officeId"]
            {
                self.myOffice = officeID
            }
        }
    }
    
    //MARK: - Notifications
    
    func opDidUpdate(_ info: [String : Any])
    {
        if UIApplication.shared.applicationState == .background
        {
            let id = info["operatoryId"] as! String
            let currentProcedureDescription = info["procedureCode"] as! String
            let currentStatus = info["status"] as! String
            let lastUpdated = info["createdAt"] as! String
            
            let opDict = [id:["id":id,"currentProcedureDescription":currentProcedureDescription,"currentStatus":currentStatus,"lastUpdated":Double(lastUpdated)!]]
            parseOpStatus(opStatusDict: opDict as NSDictionary)
        }
        
        //print(info)
    }
    
    func watchCommsDidUpdateSendData(_ info: [AnyHashable : Any])
    {
        getOpStatusSnapshot()
    }
    
    func watchCommsDidUpdateInfo(_ info: [String : AnyObject])
    {
        getOpStatusSnapshot()
    }
    
    
    //MARK: - Registger APNS (for intitial testing only)
    
    func registerAPNS()
    {
        if self.myOffice != nil
        {
            if let token = UserDefaults.standard.string(forKey: "apnsToken")
            {
                if let userID = FirebaseHelper.getUserID()
                {
                    let saveData = [userID:token]
                 
                    let path = "DeviceRegistrationTokensByOffice/\(self.myOffice!)/"
                
                    FirebaseHelper.writeRecord(withPath: path, forData: saveData, usingProtocol: self)
                    Utilities.displayAlert(title: "APNS Token", msg: "Successfully stored APNS token!", controller: self)
                }
            }
            else
            {
                Utilities.displayAlert(title: "APNS Token", msg: "No stored APNS token found. Cannot register null token.", controller: self)
            }
        }
        else
        {
            Utilities.displayAlert(title: "Office Error", msg: "No office found. Cannot register APNS token.", controller: self)
        }
    }
    
    //MARK: - Utilities
    
    func adjustLayoutForPlus()
    {
        if self.view.frame.height > 667
        {
            let differenceFromOriginal = self.view.frame.size.height - 667
            let adjustedForLegend = differenceFromOriginal + self.legendStackView.frame.size.height
            
            self.secondRowTopContraint.constant = adjustedForLegend
            self.thirdRowTopContraint.constant = adjustedForLegend
            self.legendLeftConstraint.constant = 25
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
