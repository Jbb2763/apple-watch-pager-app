//
//  Utilities.swift
//  Created by Scott Brower, AppSwage LLC on 12/14/16.

import Foundation
import UIKit
import AVFoundation

extension Date
{
    static func unversalDateFromString(dateStr:String) -> TimeInterval?
    {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        let date = formatter.date(from: dateStr)
        
        return date?.timeIntervalSince1970
    }
    
    static func stringFromUniversalDate(date: TimeInterval) -> String
    {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
    
        return formatter.string(from: Date(timeIntervalSince1970: date))
    }
}

extension UIImage
{
    func addText(_ drawText: NSString, atPoint: CGPoint, textColor: UIColor, textFont: UIFont) -> UIImage
    {
        // Setup the image context using the passed image
        UIGraphicsBeginImageContext(size)
        
        // Setup the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
        
        // Put the image into a rectangle as large as the original image
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        // Create a point within the space that is as bit as the image
        let rect = CGRect(x: atPoint.x, y: atPoint.y, width: size.width, height: size.height)
        
        // Draw the text into an image
        drawText.draw(in: rect, withAttributes: textFontAttributes)
        
        // Create a new image out of the images we have created
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //Pass the image back up to the caller
        return newImage!
        
    }
}

extension UIImage
{
    class func imageFromLabel(label: UILabel) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0.0)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

extension UIImage
{
    func resizeWith(percentage: CGFloat) -> UIImage?
    {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}

extension UIView
{
    func screenshot() -> UIImage
    {
        if(self is UIScrollView)
        {
            let scrollView = self as! UIScrollView
            
            let savedContentOffset = scrollView.contentOffset
            let savedFrame = scrollView.frame
            
            UIGraphicsBeginImageContext(scrollView.contentSize)
            scrollView.contentOffset = .zero
            self.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
            self.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext();
            
            scrollView.contentOffset = savedContentOffset
            scrollView.frame = savedFrame
            
            return image!
        }
        
        UIGraphicsBeginImageContext(self.bounds.size)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
        
    }
}

extension UIView
{
    func copyView<T: UIView>() -> T
    {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}


class Utilities: NSObject
{

    static func convertToLocalCurrency(number:Double) -> String
    {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.generatesDecimalNumbers = false
        currencyFormatter.maximumFractionDigits = 0
        currencyFormatter.locale = Locale.current
        
        return currencyFormatter.string(from: number as NSNumber)!
        
    }
    
    static func displayAlert(title:String, msg:String, controller:UIViewController)
    {
        DispatchQueue.main.async
        {
            let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            controller.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func combineImages(images:[UIImage]) -> UIImage
    {
        var maxHeight:CGFloat = 0.0
        var maxWidth:CGFloat = 0.0
        
        for image in images
        {
            maxHeight += image.size.height
            if image.size.width > maxWidth
            {
                maxWidth = image.size.width
            }
        }
        
        let finalSize = CGSize(width: maxWidth, height: maxHeight)
        
        UIGraphicsBeginImageContext(finalSize)
        
        var runningHeight: CGFloat = 0.0
        
        for image in images
        {
            image.draw(in: CGRect(x: 0.0, y: runningHeight, width: image.size.width, height: image.size.height))
            runningHeight += image.size.height
        }
        
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return finalImage!
    }
}

