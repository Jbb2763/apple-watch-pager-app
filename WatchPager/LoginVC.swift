//
//  LoginVC.swift
//  WatchPager
//
//  Created by Scott Brower, Appswage LLC on 6/17/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginVC: UIViewController, UITextFieldDelegate
{
    
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user != nil
            {
                self.performSegue(withIdentifier: "dashboardSegue", sender: self)
            }
        })

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.endEditing(true)
        return true
    }
    
    @IBAction func backgroundTapped(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    //MARK: - UI
    
    @IBAction func loginButtonTapped(_ sender: Any)
    {
        Auth.auth().signIn(withEmail: self.emailAddress.text!, password: self.password.text!, completion: { (user, error) in
            if user == nil
            {
                let alert = UIAlertController(title: "Login Error", message: error?.localizedDescription , preferredStyle: .alert)
                let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                
                alert.addAction(okButton)
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.performSegue(withIdentifier: "dashboardSegue", sender: self)
            }
        })
    }
    
    @IBAction func signupButtonTapped(_ sender: Any)
    {
        Auth.auth().createUser(withEmail: self.emailAddress.text!, password: self.password.text!, completion: { (user, error) in
            if user == nil
            {
                let alert = UIAlertController(title: "Sign-up Error", message: error?.localizedDescription , preferredStyle: .alert)
                let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
                
                alert.addAction(okButton)
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.performSegue(withIdentifier: "dashboardSegue", sender: self)
            }
        })
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
