//
//  WatchCommsModel.swift
//
//  Created by Scott Brower, AppSwage LLC on 6/15/16.


import WatchConnectivity

@objc protocol WatchCommsProtocol
{
    @objc optional func watchCommsDidUpdateInfo(_ info:[String:AnyObject])
    @objc optional func watchCommsDidUpdateSendData(_ info:[AnyHashable: Any])
}

class WatchCommsModel: NSObject, WCSessionDelegate
{
    static let sharedWatchComms = WatchCommsModel()
    fileprivate let session = WCSession.default()
    var watchCommsProtocols = [WatchCommsProtocol]()
    private let packageType = "binary"
    
    fileprivate override init()
    {
        super.init()
    }
    
    func startWatchSession()
    {
        if WCSession.isSupported()
        {
            session.delegate = self
            session.activate()
        }
    }
    
    // MARK: - Data Transfer
    
    func sendData(_ data:[String : Any])
    {
        if self.session.isReachable && self.session.activationState == .activated
        {
            let data = NSKeyedArchiver.archivedData(withRootObject: data)
            self.session.sendMessageData(data, replyHandler: nil, errorHandler: { (error) in
                print(error)
            })
        }
        else if self.session.activationState == .activated
        {
            let data = NSKeyedArchiver.archivedData(withRootObject: data)
            sendContext([self.packageType:data])
            //self.session.transferUserInfo([self.packageType:data])
        }
        else
        {
            print("Activation state is inactive!")
        }
    }
    
    func transferInfo(_ data:[String: Any])
    {
        if self.session.activationState == .activated
        {
            self.session.transferUserInfo(data)
        }
        else
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "alertError"), object: self, userInfo: ["error":"Failed to transfer"])
        }
    }
    
    func sendContext(_ data:[String : Any])
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: data)
        
        do
        {
            try self.session.updateApplicationContext([self.packageType:data])
        }catch
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "alertError"), object: self, userInfo: ["error":"Failed to send context"])
        }
    }
    
    // MARK: - Transfer Delegates
    
    func session(_ session: WCSession, didReceiveMessageData messageData: Data)
    {
        DispatchQueue.main.async
            {
                let data = NSKeyedUnarchiver.unarchiveObject(with: messageData)
                for delegate in self.watchCommsProtocols
                {
                    delegate.watchCommsDidUpdateSendData!(data as! [AnyHashable : Any])
                }
        }
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any])
    {
        let data = NSKeyedUnarchiver.unarchiveObject(with: applicationContext[packageType] as! Data)
        
        for delegate in self.watchCommsProtocols
        {
            delegate.watchCommsDidUpdateSendData!(data as! [AnyHashable : Any])
        }
    }
    
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any])
    {
        DispatchQueue.main.async
            {
                let data = NSKeyedUnarchiver.unarchiveObject(with: userInfo[self.packageType] as! Data)
                
                for delegate in self.watchCommsProtocols
                {
                    delegate.watchCommsDidUpdateSendData!(data as! [AnyHashable : Any])
                }
        }
        
    }
    
    // MARK: - Reachability Delegate
    
    func sessionReachabilityDidChange(_ session: WCSession)
    {
        #if os(iOS)
            if session.isWatchAppInstalled
            {
                processReachability()
            }
            else
            {
                print ("Watch App Not Installed")
                NotificationCenter.default.post(name: Notification.Name("noWatchApp"), object: nil)
            }
        #else
            processReachability()
        #endif
    }
    
    func processReachability()
    {
        if !session.isReachable
        {
            print ("Lost watch connectivity")
            NotificationCenter.default.post(name: Notification.Name("lostWatchConnectivity"), object: nil)
        }
        else
        {
            print ("Regained watch connectivity")
            NotificationCenter.default.post(name: Notification.Name("regainedWatchConnectivity"), object: nil)
        }
    }
    
    @available(iOS 9.3, *)
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?)
    {
        print ("Session Activated")
    }
    
    #if os(iOS)
    func sessionDidBecomeInactive(_ session: WCSession)
    {
        print ("Stopping watch communications")
    }
    
    func sessionDidDeactivate(_ session: WCSession)
    {
        print ("Watch communication has ended")
    }
    #endif
    
    #if os(iOS)
    func sessionWatchStateDidChange(_ session: WCSession)
    {
        print("Session State Changed to: \(session.activationState.rawValue)")
    }
    #endif
    
    
    // MARK: - Utilities
    
    func checkReachability() -> Bool
    {
        return self.session.isReachable
    }
    
    func addDelegate(_ me:WatchCommsProtocol)
    {
        var foundDelegate = false
        
        for delegate in self.watchCommsProtocols
        {
            if delegate === me
            {
                foundDelegate = true
                break
            }
        }
        
        if !foundDelegate
        {
            self.watchCommsProtocols.append(me)
        }
    }
}
