//
//  AppDelegate.swift
//  WatchPager
//
//  Created by Scott Brower on 5/20/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications

@objc protocol OpUpdateProtocol
{
    @objc optional func opDidUpdate(_ info:[String:Any])
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate
{

    var window: UIWindow?
    var opUpdateDelegate:OpUpdateProtocol!
    var apnsStatus = APNStatus()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        FirebaseApp.configure()
        WatchCommsModel.sharedWatchComms.startWatchSession()
        
        registerForPushNotifications()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - Push Notifications
    
    func registerForPushNotifications()
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound,.alert])
        {
            (granted, error) in
            //print("Permission granted: \(granted)")
        
            if !granted
            {
                self.apnsStatus = .notGranted
            }
            else
            {
                self.apnsStatus = .success
                self.getNotificationSettings()
            }
        }
    }
    
    func getNotificationSettings()
    {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            //print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        if let token = Messaging.messaging().fcmToken
        {
            if let existingToken = UserDefaults.standard.string(forKey: "apnsToken")
            {
                if existingToken != token
                {
                    storeAPNToken(token: token)
                }
                else
                {
                    self.apnsStatus = .match
                }
            }
            else
            {
                storeAPNToken(token: token)
            }
        }
        else
        {
            self.apnsStatus = .noToken
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Failed to register: \(error)")
        self.apnsStatus = .regFailed
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print("Silent notification received!")
        
        //self.opUpdateDelegate.opDidUpdate!(userInfo as! [String : Any])
        completionHandler(.newData)
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        storeAPNToken(token: fcmToken)
    }
    
    func storeAPNToken(token:String)
    {
        UserDefaults.standard.set(token, forKey: "apnsToken")
        UserDefaults.standard.synchronize()
        let notificationName = Notification.Name("apnsRegistered")
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    


}

