//
//  OpsPhoneModel.swift
//  WatchPager
//
//  Created by Scott Brower, AppSwage LLC on 7/21/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import UIKit

class OpsPhoneModel: NSObject
{
    var opName:String!
    var opImageView:UIImageView!
    var opMessageView:UIImageView!
    var opTimerLabel:UILabel!
    var opTimer = Timer()
    var opStatus:OpStatus! {
        didSet{
            self.updateOp()
        }
    }
    
    override init()
    {
        super.init()
    }
    
    func setEmptyOp()
    {
        self.opTimer.invalidate()
        self.opImageView.image = UIImage(named: "gray")
        
        let opNameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: opImageView.frame.size.width, height: opImageView.frame.size.height))
        opNameLabel.text = self.opName
        opNameLabel.frame.origin.y -= 9
        opNameLabel.textAlignment = .center
        opNameLabel.textColor = UIColor.white
        self.opImageView.addSubview(opNameLabel)
        
        self.opTimerLabel.frame = CGRect(x: 0, y: 0, width: opImageView.frame.size.width, height: opImageView.frame.size.height)
        self.opTimerLabel.frame.origin.y += 9
        self.opTimerLabel.text = "EMPTY"
        self.opTimerLabel.font = UIFont.systemFont(ofSize: 14.0)
        self.opTimerLabel.textAlignment = .center
        self.opImageView.addSubview(opTimerLabel)
        
        updateOpMessage(msg: "")
    }
    
    private func updateOpMessage(msg:String)
    {
        let msgLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.opMessageView.frame.size.width, height: self.opMessageView.frame.size.height))
        msgLabel.font = UIFont.systemFont(ofSize: 12)
        msgLabel.text = msg
        msgLabel.frame.origin.y += 2
        msgLabel.textAlignment = .center
        msgLabel.textColor = UIColor.black
        
        for view in self.opMessageView.subviews
        {
            view.removeFromSuperview()
        }
        
        self.opMessageView.addSubview(msgLabel)
    }
    
    private func setTimer()
    {
        self.opTimer.invalidate()
        self.opTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            let startTime = self.opStatus.updated
            let currentTimeInterval = Date().timeIntervalSince1970
            var elapsedTime = currentTimeInterval - startTime >= 0 ? currentTimeInterval - startTime : 0
            let totalSeconds = Int(elapsedTime)
            
            var hours = UInt8(elapsedTime / 3600)
            elapsedTime -= (TimeInterval(hours)*3600)
            
            var minutes = UInt8(elapsedTime / 60.0)
            elapsedTime -= (TimeInterval(minutes)*60)
            
            var seconds = UInt8(round(elapsedTime))
            
            elapsedTime -= TimeInterval(seconds)
            
            if seconds == 60
            {
                seconds = 0
                minutes += 1
            }
            
            if minutes == 60
            {
                minutes = 0
                hours += 1
            }
            
            let strSeconds = String(format: "%02d", seconds)
            
            self.opTimerLabel.text = "\(minutes)m \(strSeconds)s"
            
            self.progressUpdate(seconds: totalSeconds)
        })
    }
    
    private func progressUpdate(seconds:Int)
    {
        let segmentUpdatedAt = 30 // seconds
        var showSegment = seconds / segmentUpdatedAt
        
        if self.opStatus.status == "ready"
        {
            showSegment = showSegment < 41 ? showSegment : 40
            let currentImage = UIImage(named: "DG-App-Progress-Green-\(showSegment)")
            self.opImageView.image = currentImage
        }
        else if opStatus.status == "pending"
        {
            showSegment = showSegment < 121 ? showSegment : 120
            let currentImage = UIImage(named: "DG-App-Progress-Yellow-\(showSegment)")
            self.opImageView.image = currentImage
        }
    }
    
    private func updateOp()
    {
        if self.opStatus.status == "ready" || self.opStatus.status == "pending"
        {
            updateOpMessage(msg: self.opStatus!.procedureCode)
            setTimer()
        }
        else
        {
            setEmptyOp()
        }
    }

}
