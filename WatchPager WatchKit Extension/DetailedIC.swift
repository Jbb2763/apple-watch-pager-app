//
//  DetailedIC.swift
//  WatchPager
//
//  Created by Scott Brower, AppSwage LLC on 5/26/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import WatchKit
import Foundation


class DetailedIC: WKInterfaceController
{
    @IBOutlet var progressGroup: WKInterfaceGroup!
    @IBOutlet var opNumberLabel: WKInterfaceLabel!
    @IBOutlet var procCodesLabel: WKInterfaceLabel!
    @IBOutlet var procDescriptionLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?)
    {
        super.awake(withContext: context)
        
        if let opStatus = context as? OpStatus
        {
            let opsModel = OpsWatchModel()
            opsModel.opGroup = self.progressGroup
            opsModel.opLabel = self.opNumberLabel
            opsModel.opProcId = self.procCodesLabel
            opsModel.opProcDescription = self.procDescriptionLabel
            opsModel.opStatus = opStatus
        }
    }

    override func willActivate()
    {
        super.willActivate()
    }

    override func didDeactivate()
    {
        super.didDeactivate()
    }

}
