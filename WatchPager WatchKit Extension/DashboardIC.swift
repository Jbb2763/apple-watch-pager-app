//
//  DashboardIC.swift
//  WatchPager
//
//  Created by Scott Brower, AppSwage LLC on 5/25/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import WatchKit
import Foundation


class DashboardIC: WKInterfaceController, WatchCommsProtocol
{

    @IBOutlet var group1: WKInterfaceGroup!
    @IBOutlet var group2: WKInterfaceGroup!
    @IBOutlet var group3: WKInterfaceGroup!
    @IBOutlet var group4: WKInterfaceGroup!
    @IBOutlet var group5: WKInterfaceGroup!
    @IBOutlet var group6: WKInterfaceGroup!
    @IBOutlet var group7: WKInterfaceGroup!
    @IBOutlet var group8: WKInterfaceGroup!
    @IBOutlet var group9: WKInterfaceGroup!
    @IBOutlet var op1Label: WKInterfaceLabel!
    @IBOutlet var op2Label: WKInterfaceLabel!
    @IBOutlet var op3Label: WKInterfaceLabel!
    @IBOutlet var op4Label: WKInterfaceLabel!
    @IBOutlet var op5Label: WKInterfaceLabel!
    @IBOutlet var op6Label: WKInterfaceLabel!
    @IBOutlet var op7Label: WKInterfaceLabel!
    @IBOutlet var op8Label: WKInterfaceLabel!
    @IBOutlet var op9Label: WKInterfaceLabel!
    
    var opsDict = [String:OpsWatchModel]()
    
    override func awake(withContext context: Any?)
    {
        super.awake(withContext: context)
        
        loadOpsModel()
    }

    override func willActivate()
    {
        super.willActivate()
        
        WatchCommsModel.sharedWatchComms.addDelegate(self)
        
        askForUpdate()
    }

    override func didDeactivate()
    {
        super.didDeactivate()
    }
    
    func askForUpdate()
    {
        WatchCommsModel.sharedWatchComms.sendData(["update":1])
    }
    
    func loadOpsModel()
    {
        let ops1Model = OpsWatchModel()
        ops1Model.opName = "OP 1"
        ops1Model.opGroup = self.group1
        ops1Model.opLabel = self.op1Label
        self.opsDict["op1"] = ops1Model
        
        let ops2Model = OpsWatchModel()
        ops2Model.opName = "OP 2"
        ops2Model.opGroup = self.group2
        ops2Model.opLabel = self.op2Label
        self.opsDict["op2"] = ops2Model
        
        let ops3Model = OpsWatchModel()
        ops3Model.opName = "OP 3"
        ops3Model.opGroup = self.group3
        ops3Model.opLabel = self.op3Label
        self.opsDict["op3"] = ops3Model
        
        let ops4Model = OpsWatchModel()
        ops4Model.opName = "OP 4"
        ops4Model.opGroup = self.group4
        ops4Model.opLabel = self.op4Label
        self.opsDict["op4"] = ops4Model
        
        let ops5Model = OpsWatchModel()
        ops5Model.opName = "OP 5"
        ops5Model.opGroup = self.group5
        ops5Model.opLabel = self.op5Label
        self.opsDict["op5"] = ops5Model
        
        let ops6Model = OpsWatchModel()
        ops6Model.opName = "OP 6"
        ops6Model.opGroup = self.group6
        ops6Model.opLabel = self.op6Label
        self.opsDict["op6"] = ops6Model
        
        let ops7Model = OpsWatchModel()
        ops7Model.opName = "OP 7"
        ops7Model.opGroup = self.group7
        ops7Model.opLabel = self.op7Label
        self.opsDict["op7"] = ops7Model
        
        let ops8Model = OpsWatchModel()
        ops8Model.opName = "OP 8"
        ops8Model.opGroup = self.group8
        ops8Model.opLabel = self.op8Label
        self.opsDict["op8"] = ops8Model
        
        let ops9Model = OpsWatchModel()
        ops9Model.opName = "OP 9"
        ops9Model.opGroup = self.group9
        ops9Model.opLabel = self.op9Label
        self.opsDict["op9"] = ops9Model
        
        //initOps()
    }
    
    func initOps()
    {
        for (_,opsModel) in self.opsDict
        {
            opsModel.setEmptyOp()
        }
    }
    
    // MARK: - Watch Communications
    
    func watchCommsDidUpdateSendData(_ info: [AnyHashable : Any])
    {
        if info["opStatus"] != nil
        {
            let opsArray = info["opStatus"] as! [[String:Any]]
            
            for opStatusDict in opsArray
            {
                let opStatus = OpStatus.dictToStruct(opStatusDict: opStatusDict)
                let opsModel = self.opsDict[opStatus.id.lowercased()]
                opsModel?.opStatus = opStatus
            }
        }
    }
    
    // MARK: - Navigation
    
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any?
    {
       switch segueIdentifier
       {
            case "op1Segue":
                return self.opsDict["op1"]?.opStatus
            case "op2Segue":
                return self.opsDict["op2"]?.opStatus
            case "op3Segue":
                return self.opsDict["op3"]?.opStatus
            case "op4Segue":
                return self.opsDict["op4"]?.opStatus
            case "op5Segue":
                return self.opsDict["op5"]?.opStatus
            case "op6Segue":
                return self.opsDict["op6"]?.opStatus
            case "op7Segue":
                return self.opsDict["op7"]?.opStatus
            case "op8Segue":
                return self.opsDict["op8"]?.opStatus
            case "op9Segue":
                return self.opsDict["op9"]?.opStatus
            default: break
       }
        
        return nil
    }
}
