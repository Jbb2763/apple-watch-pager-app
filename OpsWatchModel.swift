//
//  OpsWatchModel.swift
//  WatchPager
//
//  Created by Scott Brower, AppSwage LLC on 7/21/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import WatchKit

class OpsWatchModel: NSObject
{
    var opName:String!
    var opGroup:WKInterfaceGroup!
    var opLabel:WKInterfaceLabel!
    var opProcDescription:WKInterfaceLabel?
    var opProcId:WKInterfaceLabel?
    var opTimer = Timer()
    var previousTimerSegment = 0
    var opStatus:OpStatus! {
        didSet{
            self.updateOp()
        }
    }
    
    func setEmptyOp()
    {
        self.opTimer.invalidate()
        self.opGroup.setBackgroundImageNamed("gray0")
        self.opLabel.setText(OpStatus.getRoomNumber(id: self.opStatus.id))
        let color = UIColor(colorLiteralRed: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        self.opLabel.setTextColor(color)
        setProcedure(color: color)
    }
    
    func updateOp()
    {
        DispatchQueue.main.async(execute: {
            self.previousTimerSegment = 0
            
            switch self.opStatus.status
            {
            case "ready":
                self.opGroup.setBackgroundImageNamed("green")
                self.opLabel.setText(OpStatus.getRoomNumber(id: self.opStatus.id))
                let color = UIColor(colorLiteralRed: 0/255, green: 255/255, blue: 127/255, alpha: 1)
                self.opLabel.setTextColor(color)
                self.setProcedure(color: color)
                self.setTimer()
            case "pending":
                self.opGroup.setBackgroundImageNamed("yellow")
                self.opLabel.setText(OpStatus.getRoomNumber(id: self.opStatus.id))
                let color = UIColor(colorLiteralRed: 255/255, green: 247/255, blue: 10/255, alpha: 1)
                self.opLabel.setTextColor(color)
                self.setProcedure(color: color)
                self.setTimer()
            case "empty":
                self.setEmptyOp()
            default:
                break
            }
        })
    }
    
    private func setProcedure(color:UIColor)
    {
        if self.opProcId != nil
        {
            self.opProcId?.setText(self.opStatus.procedureCode)
            self.opProcDescription?.setText(self.opStatus.procedureDescription)
            self.opProcId?.setTextColor(color)
            self.opProcDescription?.setTextColor(color)
        }
    }
    
    private func setTimer()
    {
        self.opTimer.invalidate()
        self.opTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            let startTime = self.opStatus.updated
            let currentTimeInterval = Date().timeIntervalSince1970
            var elapsedTime = currentTimeInterval - startTime >= 0 ? currentTimeInterval - startTime : 0
            let totalSeconds = Int(elapsedTime)
            
            var hours = UInt8(elapsedTime / 3600)
            elapsedTime -= (TimeInterval(hours)*3600)
            
            var minutes = UInt8(elapsedTime / 60.0)
            elapsedTime -= (TimeInterval(minutes)*60)
            
            var seconds = UInt8(round(elapsedTime))
            
            elapsedTime -= TimeInterval(seconds)
            
            if seconds == 60
            {
                seconds = 0
                minutes += 1
            }
            
            if minutes == 60
            {
                minutes = 0
                hours += 1
            }
            
            self.progressUpdate(seconds: totalSeconds)
        })
    }
    
    private func progressUpdate(seconds:Int)
    {
        let segmentUpdatedAt = 60 // seconds
        var showSegment = seconds / segmentUpdatedAt
        
        if self.previousTimerSegment != showSegment
        {
            self.previousTimerSegment = self.previousTimerSegment == 0 ? 1 : self.previousTimerSegment
            if self.opStatus.status == "ready"
            {
                showSegment = showSegment < 21 ? showSegment : 20
                self.opGroup.setBackgroundImageNamed("green\(showSegment)")
            }
            else if self.opStatus.status == "pending"
            {
                showSegment = showSegment < 61 ? showSegment : 60
                self.opGroup.setBackgroundImageNamed("yellow\(showSegment)")
            }
        }
        
        self.previousTimerSegment = showSegment
    }

    
}
