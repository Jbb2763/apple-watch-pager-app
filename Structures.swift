//
//  Structures.swift
//  WatchPager
//
//  Created by Scott Brower, AppSwage LLC on 6/24/17.
//  Copyright © 2017 James Biswell. All rights reserved.
//

import Foundation

// MARK: - Structures

struct OpStatus
{
    var id:String
    var procedureCode:String
    var procedureDescription:String
    var status:String
    var updated:Double
    
    static func strucToDict(opStatus:OpStatus) -> [String:Any]
    {
        return ["id":opStatus.id,"procedureCode":opStatus.procedureCode,"procedureDescription":opStatus.procedureDescription,"status":opStatus.status,"updated":opStatus.updated]
    }
    
    static func dictToStruct(opStatusDict:[String:Any]) -> OpStatus
    {
        return OpStatus(id: opStatusDict["id"] as! String, procedureCode: opStatusDict["procedureCode"] as! String, procedureDescription: opStatusDict["procedureDescription"] as! String, status: opStatusDict["status"] as! String, updated: opStatusDict["updated"] as! Double)
    }
    
    static func getRoomNumber(id:String) -> String
    {
        let startIndex = id.index(id.startIndex, offsetBy: 2)
        return id.substring(from: startIndex)
    }
}

//MARK: - Enums

enum RoomStatus
{
    case pending
    case empty
    case ready
}

enum APNStatus: String
{
    case success = "Success"
    case notGranted = "Notificiations not granted"
    case regFailed = "Failed registration"
    case noToken = "Cannot get APNS token"
    case match = "Store and FB tokens match. No update required."
    case notDeternined = "Undefined"
    
    init()
    {
        self = .notDeternined
    }
    
}
